
FLYWAY_URL="jdbc:postgresql://${POSTGRES_ADDRESS}:${POSTGRES_PORT}/${POSTGRES_DATABASE}"

echo "--- Install Flyway locally ---"
wget -qO- https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/9.4.0/flyway-commandline-9.4.0-linux-x64.tar.gz | tar xz && ln -s "$(pwd)"/flyway-9.4.0/flyway /usr/local/bin
mv "postgresql-42.5.0.jar" "flyway-9.4.0/drivers/redshift-jdbc42-2.1.0.9.jar"

echo "--- Run Flyway info + migration ---"
flyway info -url="${FLYWAY_URL}" -user="${POSTGRES_USERNAME}" -password="${POSTGRES_PASSWORD}" -locations="filesystem:${FLYWAY_FOLDER_LOCATION}" -baselineOnMigrate=true -baselineVersion="${FLYWAY_VERSION}"
flyway migrate -url="${FLYWAY_URL}" -user="${POSTGRES_USERNAME}" -password="${POSTGRES_PASSWORD}" -locations="filesystem:${FLYWAY_FOLDER_LOCATION}" -baselineOnMigrate=true -baselineVersion="${FLYWAY_VERSION}"

